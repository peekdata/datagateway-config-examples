
import { config } from 'config';
import { IServices, registerServices } from './services';
import { DataModelsApi } from 'api/dataModels';
import { ValidationApi } from 'api/validation';
import { DataApi } from 'api/data';
import { HealthcheckApi } from 'api/healthcheck';

export const bootstrapIOC = () => {
  const services: Partial<IServices> = {};

  services.api = {
    dataModels: new DataModelsApi(config.apiUrl),
    validation: new ValidationApi(config.apiUrl),
    data: new DataApi(config.apiUrl),
    healthCheck: new HealthcheckApi(config.apiUrl),
  };

  registerServices(services as IServices);
};
