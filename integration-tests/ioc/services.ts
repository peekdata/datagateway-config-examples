import { DataModelsApi } from 'api/dataModels';
import { ValidationApi } from 'api/validation';
import { DataApi } from 'api/data';
import { HealthcheckApi } from 'api/healthcheck';

let registeredServices: IServices = null;

export interface IServices {
  api: IApiServices;
}

export interface IApiServices {
  dataModels: DataModelsApi;
  data: DataApi;
  validation: ValidationApi;
  healthCheck: HealthcheckApi;
}

/**
 * Registers given services as IoC container
 */
export const registerServices = (services: IServices) => {
  registeredServices = services;
};

/**
 * Gets IoC services container
 */
export const getServices = (): IServices => {
  return registeredServices;
};
