import { request, use } from 'chai';
import chaiHttp from 'chai-http';
import { IDataRequest } from './models/dataRequest';
import { ITrackedResponse, Response } from './models/response';

use(chaiHttp);

// #region -------------- Interfaces -------------------------------------------------------------------

export interface IDataResult<TRow> extends ITrackedResponse {
  columnHeaders: IColumnHeader[];
  rows: TRow[];
  totalRows: number;
}

export interface IColumnHeader {
  title: string;
  name: string;
  dataType: string;
  format: string;
  alias: string;
  columnType: string;
}

export type FullRow = { [key: string]: any };
export type OptimizedRow = any[];

// #endregion

export class DataApi {
  public constructor(private apiUrl) { }

  public async getQuery(dataRequest: IDataRequest): Promise<Response<string>> {
    return request(this.apiUrl)
      .post('/v1/query/get')
      .set('Content-Type', 'application/json')
      .send(dataRequest);
  }

  public async get(dataRequest: IDataRequest): Promise<Response<IDataResult<FullRow>>> {
    return request(this.apiUrl)
      .post('/v1/data/get')
      .set('Content-Type', 'application/json')
      .send(dataRequest);
  }

  public async getOptimized(dataRequest: IDataRequest): Promise<Response<IDataResult<OptimizedRow>>> {
    return request(this.apiUrl)
      .post('/v1/data/getoptimized')
      .set('Content-Type', 'application/json')
      .send(dataRequest);
  }

  public async getFile(dataRequest: IDataRequest): Promise<Response<Buffer>> {
    return request(this.apiUrl)
      .post('/v1/file/get')
      .set('Content-Type', 'application/json')
      .send(dataRequest)
      .buffer();
  }
}
