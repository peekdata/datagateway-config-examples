export interface IDataPoint {
  name: string;
  title: string;
  description: string;
  groups: string[];
  dataType: string;
  sortingOrder: number;
}
