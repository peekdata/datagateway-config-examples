export interface IDataRequest {
  requestID?: string;
  consumerInfo?: string;
  scopeName: string;
  dataModelName?: string;
  dimensions: string[];
  metrics: string[];
  securityRole?: string;
  sortings?: ISortings;
  filters?: IFilters;
  options?: IOptions;
}

// #region -------------- Sortings -------------------------------------------------------------------

export interface ISortings {
  metrics?: ISorting[];
  dimensions?: ISorting[];
}

export interface ISorting {
  direction?: string;
  key: string;
}

// #endregion

// #region -------------- Filters -------------------------------------------------------------------

export interface IFilters {
  singleValues?: ISingleValueFilter[];
  singleKeys?: ISingleKeysFilter[];
  dateRanges?: IDateRangeFilter[];
}

export interface ISingleValueFilter {
  keys: string[];
  operation: string;
  value: any;
}

export interface ISingleKeysFilter {
  key: string;
  operation: string;
  values: any[];
}

export interface IDateRangeFilter {
  key: string;
  from?: string;
  to?: string;
}

// #endregion

// #region -------------- Options -------------------------------------------------------------------

export interface IOptions {
  rows?: IPaging;
  arguments?: { [key: string]: string };
  connections?: IConnection;
}

export interface IPaging {
  limitRowsTo?: number;
  startWithRow?: number;
}

export interface IConnection {
  type: string;
  name: string;
}

export enum ConnectionType {
  Default = 'DEFAULT',
  Named = 'NAMED',
}

// #endregion
