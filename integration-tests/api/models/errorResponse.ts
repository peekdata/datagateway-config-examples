import { ITrackedResponse } from './response';

export interface IErrorResponse {
  timestamp: string;
  status: number;
  error: string;
  message: string;
  path: string;
}

export interface IPeekdataErrorResponse extends ITrackedResponse {
  errorCode: number;
  message: string;
}
