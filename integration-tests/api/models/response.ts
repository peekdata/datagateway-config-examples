import * as req from 'superagent';

export interface Response<TBody> extends req.Response {
  body: TBody;
}

export interface ITrackedResponse {
  requestID: string;
}
