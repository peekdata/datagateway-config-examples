import { request, use } from 'chai';
import chaiHttp from 'chai-http';
import { Response } from './models/response';

use(chaiHttp);

// #region -------------- Interfaces -------------------------------------------------------------------

export interface IHealthcheckResponse {
  message: string;
}

export interface IHealthcheckOverallResponse {
  scopes: IScopeStatus[];
  overallStatus: HealthCheckStatus;
}

export interface IScopeStatus {
  name: string;
  overallStatus: HealthCheckStatus;
  dataModels: IDataModelStatus[];
}

export interface IDataModelStatus {
  name: string;
  dataModelStatus: HealthCheckStatus;
  databaseStatus: HealthCheckStatus;
}

export enum HealthCheckStatus {
  Ok = 'OK',
  Failed = 'Failed',
}

// #endregion

export class HealthcheckApi {
  public constructor(private apiUrl) { }

  public async getHealthcheck(): Promise<Response<IHealthcheckResponse>> {
    return request(this.apiUrl)
      .get('/v1/healthcheck')
      .send();
  }

  public async getHealthcheckOverall(): Promise<Response<IHealthcheckOverallResponse>> {
    return request(this.apiUrl)
      .get('/v1/healthcheck/overall')
      .send();
  }

  public async getHealthcheckDataModels(): Promise<Response<{ [key: string]: HealthCheckStatus }>> {
    return request(this.apiUrl)
      .get('/v1/healthcheck/datamodels')
      .send();
  }

  public async getHealthcheckConnections(): Promise<Response<{ [key: string]: HealthCheckStatus }>> {
    return request(this.apiUrl)
      .get('/v1/healthcheck/connections')
      .send();
  }
}
