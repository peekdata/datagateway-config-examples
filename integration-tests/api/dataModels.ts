import { request, use } from 'chai';
import chaiHttp from 'chai-http';
import { Response } from './models/response';
import { IDataPoint } from './models/dataPoint';

use(chaiHttp);

export class DataModelsApi {
  public constructor(private apiUrl) { }

  public async getScopeNames(): Promise<Response<string[]>> {
    return request(this.apiUrl)
      .get('/v1/datamodel/getscopenames')
      .set('Content-Type', 'application/json');
  }

  public async getDataModelNames(scopeName: string): Promise<Response<string[]>> {
    return request(this.apiUrl)
      .get('/v1/datamodel/getdatamodelnames')
      .set('Content-Type', 'application/json')
      .query({ scopeName });
  }

  public async getCubes(scopeName: string, dataModelName: string): Promise<Response<string[]>> {
    return request(this.apiUrl)
      .get('/v1/datamodel/getcubes')
      .set('Content-Type', 'application/json')
      .query({ scopeName, dataModelName });
  }

  public async getDimensions(scopeName: string, dataModelName: string): Promise<Response<IDataPoint[]>> {
    return request(this.apiUrl)
      .get('/v1/datamodel/getdimensions')
      .set('Content-Type', 'application/json')
      .query({ scopeName, dataModelName });
  }

  public async getMetrics(scopeName: string, dataModelName: string): Promise<Response<IDataPoint[]>> {
    return request(this.apiUrl)
      .get('/v1/datamodel/getmetrics')
      .set('Content-Type', 'application/json')
      .query({ scopeName, dataModelName });
  }
}
