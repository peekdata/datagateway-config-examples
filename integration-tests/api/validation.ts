import { request, use } from 'chai';
import chaiHttp from 'chai-http';
import { IDataRequest } from './models/dataRequest';
import { Response, ITrackedResponse } from './models/response';
import { IDataPoint } from './models/dataPoint';

use(chaiHttp);

// #region -------------- Interfaces -------------------------------------------------------------------

export interface ICompatibilityResult extends ITrackedResponse {
  consumerInfo: string;
  dimensions: IDataPoint[];
  metrics: IDataPoint[];
}

export interface IValidationResult extends ITrackedResponse {
  isValid: boolean;
}

// #endregion

export class ValidationApi {
  public constructor(private apiUrl) { }

  public async getCompatible(dataRequest: IDataRequest): Promise<Response<ICompatibilityResult>> {
    return request(this.apiUrl)
      .post('/v1/datamodel/getcompatible')
      .set('Content-Type', 'application/json')
      .send(dataRequest);
  }

  public async validate(dataRequest: IDataRequest): Promise<Response<IValidationResult>> {
    return request(this.apiUrl)
      .post('/v1/datamodel/validate')
      .set('Content-Type', 'application/json')
      .send(dataRequest);
  }
}
