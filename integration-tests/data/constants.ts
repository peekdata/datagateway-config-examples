export const northWind = {
  scope: 'Northwind Marketing',
  dataModel: 'Sales',
  cubes: {
    order: 'order',
    product: 'product',
  },
  dimensions: {
    employee_firstname: 'employee_firstname',
    employee_lastname: 'employee_lastname',
    order_shippeddate: 'order_shippeddate',
    order_ship_country: 'order_ship_country',
    order_islate: 'order_islate',
    order_freight: 'order_freight',
    supplier_companyname: 'supplier_companyname',
    product_name: 'product_name',
    customer_country: 'customer_country',
  },
  metrics: {
    totalprice: 'totalprice',
    order_orderscount: 'order_orderscount',
    order_productsalescount: 'order_productsalescount',
    product_unitprice: 'product_unitprice',
    product_unitsinstock: 'product_unitsinstock',
  },
};
