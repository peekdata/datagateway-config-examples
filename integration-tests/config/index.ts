const config = {
  apiUrl: process.env.API_URL || 'http://localhost:8080/datagateway',
};

export { config };
