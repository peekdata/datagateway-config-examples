import { Response } from 'api/models/response';

export function formatFailedResponse(response: Response<any>): string {
  if (!response) {
    return '--- NULL response ---\n';
  }

  if (response.error) {
    return `[${response.error.status}] ${response.error.text || response.error.message}\n`;
  }

  return JSON.stringify(response.body);
}
