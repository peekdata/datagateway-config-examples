import { IDataRequest } from 'api/models/dataRequest';
import { IPeekdataErrorResponse } from 'api/models/errorResponse';
import { Response } from 'api/models/response';
import { expect } from 'chai';
import { northWind } from 'data/constants';
import { bootstrapIOC } from 'ioc/bootstrapIOC';
import { getServices, IApiServices } from 'ioc/services';
import { formatFailedResponse } from 'lib/errors/formatting';
import 'mocha';

// #region -------------- Vars -------------------------------------------------------------------

let api: IApiServices;

// #endregion

// #region -------------- Before -------------------------------------------------------------------

before(async () => {
  bootstrapIOC();
  api = getServices().api;
});

// #endregion

describe('Validation API', () => {

  describe('Getting compatibility', () => {
    it('Should get with minimal request', async () => {
      const req: IDataRequest = {
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [],
        metrics: [],
      };

      const resp = await api.validation.getCompatible(req);

      const expectedDims = Object.values(northWind.dimensions);
      const expectedMetrics = Object.values(northWind.metrics);

      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.body.dimensions.map(e => e.name)).to.include.all.members(expectedDims);
      expect(resp.body.metrics.map(e => e.name)).to.include.all.members(expectedMetrics);
      expect(resp.body.requestID).to.not.be.empty;
    });

    it('Should get with some dims, metrics', async () => {
      const req: IDataRequest = {
        requestID: 'abc-123',
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [
          northWind.dimensions.employee_firstname,
          northWind.dimensions.customer_country,
        ],
        metrics: [
          northWind.metrics.order_orderscount,
          northWind.metrics.order_productsalescount,
          northWind.metrics.totalprice,
        ],
      };

      const resp = await api.validation.getCompatible(req);

      const expectedDims = [
        northWind.dimensions.employee_firstname,
        northWind.dimensions.employee_lastname,
        northWind.dimensions.supplier_companyname,
        northWind.dimensions.order_ship_country,
        northWind.dimensions.order_islate,
        northWind.dimensions.customer_country,
        northWind.dimensions.order_shippeddate,
      ];
      const expectedMetrics = [
        northWind.metrics.totalprice,
        northWind.metrics.order_orderscount,
        northWind.metrics.order_productsalescount,
      ];

      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.body.dimensions.map(e => e.name)).to.include.all.members(expectedDims);
      expect(resp.body.metrics.map(e => e.name)).to.include.all.members(expectedMetrics);
      expect(resp.body.requestID).to.eq(req.requestID);
    });

    it('Should NOT get with incompatible dims', async () => {
      const req: IDataRequest = {
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [
          northWind.dimensions.employee_firstname,
          northWind.dimensions.product_name,
        ],
        metrics: [
        ],
      };

      const resp = await api.validation.getCompatible(req) as any as Response<IPeekdataErrorResponse>;

      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.body.errorCode).to.equal(57010, formatFailedResponse(resp));
      expect(resp.body.message).to.not.be.empty;
    });

    it('Should NOT get with non-existing dims', async () => {
      const req: IDataRequest = {
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [
          'dummy',
        ],
        metrics: [
        ],
      };

      const resp = await api.validation.getCompatible(req) as any as Response<IPeekdataErrorResponse>;

      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.body.errorCode).to.equal(57010, formatFailedResponse(resp));
      expect(resp.body.message).to.not.be.empty;
    });

    it('Should NOT get with non-existing scope', async () => {
      const req: IDataRequest = {
        scopeName: 'dummy',
        dataModelName: northWind.dataModel,
        dimensions: [],
        metrics: [],
      };

      const resp = await api.validation.getCompatible(req) as any as Response<IPeekdataErrorResponse>;

      expect(resp.status).to.not.equal(200, formatFailedResponse(resp));
    });

    it('Should NOT get with non-existing data model', async () => {
      const req: IDataRequest = {
        scopeName: northWind.scope,
        dataModelName: 'dummy',
        dimensions: [],
        metrics: [],
      };

      const resp = await api.validation.getCompatible(req) as any as Response<IPeekdataErrorResponse>;

      expect(resp.status).to.not.equal(200, formatFailedResponse(resp));
    });
  });

  describe('Validation', () => {
    it('Should validate true with minimal request', async () => {
      const req: IDataRequest = {
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [],
        metrics: [],
      };

      const resp = await api.validation.validate(req);

      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.body.isValid).to.be.true;
    });

    it('Should validate true with some dims, metrics', async () => {
      const req: IDataRequest = {
        requestID: 'abc-123',
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [
          northWind.dimensions.employee_firstname,
          northWind.dimensions.customer_country,
        ],
        metrics: [
          northWind.metrics.order_orderscount,
          northWind.metrics.order_productsalescount,
          northWind.metrics.totalprice,
        ],
      };

      const resp = await api.validation.validate(req);

      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.body.isValid).to.be.true;
    });

    it('Should validate false with incompatible dims', async () => {
      const req: IDataRequest = {
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [
          northWind.dimensions.employee_firstname,
          northWind.dimensions.product_name,
        ],
        metrics: [
        ],
      };

      const resp = await api.validation.validate(req);

      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.body.isValid).to.be.false;
    });
  });
});
