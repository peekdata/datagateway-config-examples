import { expect } from 'chai';
import 'mocha';
import { bootstrapIOC } from 'ioc/bootstrapIOC';
import { getServices, IApiServices } from 'ioc/services';
import { northWind } from 'data/constants';
import { formatFailedResponse } from 'lib/errors/formatting';

// #region -------------- Vars -------------------------------------------------------------------

let api: IApiServices;

// #endregion

// #region -------------- Before -------------------------------------------------------------------

before(async () => {
  bootstrapIOC();
  api = getServices().api;
});

// #endregion

describe('Data models API', () => {
  it('Should get scope names', async () => {
    const resp = await api.dataModels.getScopeNames();

    expect(resp.status).to.equal(200, formatFailedResponse(resp));
    expect(resp.body).to.deep.equal([northWind.scope]);
  });

  it('Should get data model names', async () => {
    const resp = await api.dataModels.getDataModelNames(northWind.scope);

    expect(resp.status).to.equal(200, formatFailedResponse(resp));
    expect(resp.body).to.deep.equal([northWind.dataModel]);
  });

  it('Should get cubes', async () => {
    const resp = await api.dataModels.getCubes(northWind.scope, northWind.dataModel);

    expect(resp.status).to.equal(200, formatFailedResponse(resp));
    expect(resp.body).to.include.all.members(Object.values(northWind.cubes));
  });

  it('Should get dimensions', async () => {
    const resp = await api.dataModels.getDimensions(northWind.scope, northWind.dataModel);

    const expectedDims = Object.values(northWind.dimensions);

    expect(resp.status).to.equal(200, formatFailedResponse(resp));
    expect(resp.body.length).to.equal(expectedDims.length);
    expect(resp.body.map(e => e.name)).to.include.all.members(expectedDims);
    expect(resp.body[0]).to.have.keys(['name', 'title', 'description', 'groups', 'dataType', 'sortingOrder']);
  });

  it('Should get metrics', async () => {
    const resp = await api.dataModels.getMetrics(northWind.scope, northWind.dataModel);

    const expectedMetrics = Object.values(northWind.metrics);

    expect(resp.status).to.equal(200, formatFailedResponse(resp));
    expect(resp.body.length).to.equal(expectedMetrics.length);
    expect(resp.body.map(e => e.name)).to.include.all.members(expectedMetrics);
    expect(resp.body[0]).to.have.keys(['name', 'title', 'description', 'groups', 'dataType', 'sortingOrder']);
  });
});
