import { expect } from 'chai';
import { bootstrapIOC } from 'ioc/bootstrapIOC';
import { getServices, IApiServices } from 'ioc/services';
import { formatFailedResponse } from 'lib/errors/formatting';
import 'mocha';
import { northWind } from 'data/constants';

// #region -------------- Vars -------------------------------------------------------------------

let api: IApiServices;

// #endregion

// #region -------------- Before -------------------------------------------------------------------

before(async () => {
  bootstrapIOC();
  api = getServices().api;
});

// #endregion

describe('Healthcheck API', () => {
  it('Should get healthcheck', async () => {
    const resp = await api.healthCheck.getHealthcheck();

    expect(resp.status).to.equal(200, formatFailedResponse(resp));
    expect(resp.body.message).to.equal('Service is OK');
  });

  it('Should get overall healthcheck', async () => {
    const resp = await api.healthCheck.getHealthcheckOverall();

    expect(resp.status).to.equal(200, formatFailedResponse(resp));
    expect(resp.body).to.deep.equal({
      scopes: [
        {
          name: northWind.scope,
          overallStatus: "OK",
          dataModels: [
            {
              name: northWind.dataModel,
              dataModelStatus: "OK",
              databaseStatus: "OK"
            }
          ]
        }
      ],
      overallStatus: "OK"
    });
  });

  it('Should get datamodels healthcheck', async () => {
    const resp = await api.healthCheck.getHealthcheckDataModels();

    expect(resp.status).to.equal(200, formatFailedResponse(resp));
    expect(resp.body).to.deep.equal({
      [northWind.dataModel]: 'OK',
    });
  });
});
