import { IDataRequest } from 'api/models/dataRequest';
import { expect } from 'chai';
import { northWind } from 'data/constants';
import { bootstrapIOC } from 'ioc/bootstrapIOC';
import { getServices, IApiServices } from 'ioc/services';
import { formatFailedResponse } from 'lib/errors/formatting';
import 'mocha';

// #region -------------- Vars -------------------------------------------------------------------

let api: IApiServices;

// #endregion

// #region -------------- Before -------------------------------------------------------------------

before(async () => {
  bootstrapIOC();
  api = getServices().api;
});

// #endregion

describe('Data API', () => {

  describe('Getting query', () => {
    it('Should get with minimal request', async () => {
      const req: IDataRequest = {
        requestID: 'abc-123',
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [northWind.dimensions.order_shippeddate],
        metrics: [northWind.metrics.order_orderscount],
      };

      const expResult =
        'SELECT -- rqid=abc-123 \n' +
        '\tord.shippeddate AS order_shippeddate_1,\n' +
        '\tSUM(1) AS order_orderscount_2\n' +
        'FROM public.orders AS ord\n' +
        'GROUP BY order_shippeddate_1\n';

      const resp = await api.data.getQuery(req);
      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.text).to.equal(expResult);
    });

    it('Should get with sorting, filtering, complex dims, metrics', async () => {
      const req: IDataRequest = {
        requestID: 'abc-123',
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [
          northWind.dimensions.order_shippeddate,
          northWind.dimensions.customer_country,
          northWind.dimensions.order_islate,
          northWind.dimensions.supplier_companyname,
        ],
        metrics: [northWind.metrics.order_orderscount, northWind.metrics.totalprice],
        filters: {
          singleValues: [
            {
              keys: [northWind.dimensions.customer_country, northWind.dimensions.order_islate],
              operation: 'EQUALS',
              value: 'N',
            },
            {
              keys: [northWind.dimensions.order_shippeddate],
              operation: 'ALL_IS_MORE',
              value: '1997-11-01',
            },
          ],
          singleKeys: [
            {
              key: northWind.dimensions.customer_country,
              operation: 'STARTS_WITH',
              values: ['USA', 'Mexico', 'Brazil'],
            },
            {
              key: northWind.dimensions.customer_country,
              operation: 'NOT_STARTS_WITH',
              values: ['Brazil'],
            },
          ],
          dateRanges: [
            {
              key: northWind.dimensions.order_shippeddate,
              from: '1980-01-01',
              to: '1998-11-01',
            },
          ],
        },
        options: {
          rows: {
            startWithRow: 10,
            limitRowsTo: 5,
          },
        },
        sortings: {
          dimensions: [
            {
              key: northWind.dimensions.order_shippeddate,
              direction: "DESC"
            },
            {
              key: northWind.dimensions.customer_country,
            }
          ],
          metrics: [
            {
              key: northWind.metrics.order_orderscount,
              direction: "DESC"
            }
          ]
        }
      };

      const expResult =
        'SELECT -- rqid=abc-123 \n' +
        '\tord.shippeddate AS order_shippeddate_1,\n' +
        '\tcust.country AS customer_country_2,\n' +
        "\t(CASE WHEN requireddate < shippeddate THEN 'Y' ELSE 'N' END) AS order_islate_3,\n" +
        '\tsupp.companyname AS supplier_companyname_4,\n' +
        '\tSUM(1) AS order_orderscount_5,\n' +
        '\tSUM(ord_det.unitprice * quantity - discount) AS totalprice_6\n' +
        'FROM public.orders AS ord\n' +
        '\tJOIN public.customers cust ON cust.customerid = ord.customerid\n' +
        '\tJOIN public.order_details ord_det ON ord_det.orderid = ord.orderid\n' +
        '\tJOIN public.products prd ON prd.productid = ord_det.productid\n' +
        '\tJOIN public.suppliers supp ON supp.supplierid = prd.supplierid\n' +
        'WHERE\n' +
        "\tord.shippeddate >= '1980-01-01'\n" +
        "\tAND ord.shippeddate <= '1998-11-01'\n" +
        "\tAND (cast(cust.country as varchar) like (replace(replace(replace('USA', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!' or cast(cust.country as varchar) like (replace(replace(replace('Mexico', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!' or cast(cust.country as varchar) like (replace(replace(replace('Brazil', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!')\n" +
        "\tAND not(cast(cust.country as varchar) like (replace(replace(replace('Brazil', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!')\n" +
        "\tAND (cust.country = 'N' or (CASE WHEN requireddate < shippeddate THEN 'Y' ELSE 'N' END) = 'N')\n" +
        "\tAND ord.shippeddate > '1997-11-01'\n" +
        'GROUP BY order_shippeddate_1, customer_country_2, order_islate_3, supplier_companyname_4\n' +
        'ORDER BY order_shippeddate_1 DESC, customer_country_2, order_orderscount_5 DESC\n' +
        'LIMIT 5 OFFSET 10';

      const resp = await api.data.getQuery(req);
      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.text).to.equal(expResult);
    });

    it('Should build all types of "singleValues" filters', async () => {
      const req: IDataRequest = {
        requestID: 'abc-123',
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [
          northWind.dimensions.order_shippeddate,
          northWind.dimensions.customer_country,
          northWind.dimensions.supplier_companyname,
          northWind.dimensions.order_freight,
        ],
        metrics: [northWind.metrics.order_orderscount],
        filters: {
          singleValues: [
            {
              keys: [northWind.dimensions.order_freight],
              operation: "EQUALS",
              value: 12.34
            },
            {
              keys: [northWind.dimensions.customer_country, northWind.dimensions.supplier_companyname],
              operation: "EQUALS",
              value: "';DROPtest"
            },
            {
              keys: [northWind.dimensions.order_freight],
              operation: "NOT_EQUALS",
              value: 12.34
            },
            {
              keys: [northWind.dimensions.customer_country, northWind.dimensions.supplier_companyname],
              operation: "NOT_EQUALS",
              value: "';DROPtest"
            },
            {
              keys: [northWind.dimensions.customer_country, northWind.dimensions.supplier_companyname],
              operation: "STARTS_WITH",
              value: "%';DROPtest!"
            },
            {
              keys: [northWind.dimensions.customer_country, northWind.dimensions.supplier_companyname],
              operation: "NOT_STARTS_WITH",
              value: "%';DROPtest!"
            },
            {
              keys: [northWind.dimensions.order_shippeddate, northWind.dimensions.order_shippeddate],
              operation: "ALL_IS_LESS",
              value: "2000-10-28"
            },
            {
              keys: [northWind.dimensions.order_shippeddate, northWind.dimensions.order_shippeddate],
              operation: "ALL_IS_LESS_OR_EQUAL",
              value: "2000-10-28"
            },
            {
              keys: [northWind.dimensions.order_shippeddate, northWind.dimensions.order_shippeddate],
              operation: "ALL_IS_MORE",
              value: "2000-10-28"
            },
            {
              keys: [northWind.dimensions.order_shippeddate, northWind.dimensions.order_shippeddate],
              operation: "ALL_IS_MORE_OR_EQUAL",
              value: "2000-10-28"
            },
            {
              keys: [northWind.dimensions.order_shippeddate, northWind.dimensions.order_shippeddate],
              operation: "AT_LEAST_ONE_IS_LESS",
              value: "2000-10-28"
            },
            {
              keys: [northWind.dimensions.order_shippeddate, northWind.dimensions.order_shippeddate],
              operation: "AT_LEAST_ONE_IS_LESS_OR_EQUAL",
              value: "2000-10-28"
            },
            {
              keys: [northWind.dimensions.order_shippeddate, northWind.dimensions.order_shippeddate],
              operation: "AT_LEAST_ONE_IS_MORE",
              value: "2000-10-28"
            },
            {
              keys: [northWind.dimensions.order_shippeddate, northWind.dimensions.order_shippeddate],
              operation: "AT_LEAST_ONE_IS_MORE_OR_EQUAL",
              value: "2000-10-28"
            }
          ],
        },
      };

      const expResult =
        'SELECT -- rqid=abc-123 \n' +
        '\tord.shippeddate AS order_shippeddate_1,\n' +
        '\tcust.country AS customer_country_2,\n' +
        '\tsupp.companyname AS supplier_companyname_3,\n' +
        '\tord.freight AS order_freight_4,\n' +
        '\tSUM(1) AS order_orderscount_5\n' +
        'FROM public.orders AS ord\n' +
        '\tJOIN public.order_details ord_det ON ord_det.orderid = ord.orderid\n' +
        '\tJOIN public.products prd ON prd.productid = ord_det.productid\n' +
        '\tJOIN public.suppliers supp ON supp.supplierid = prd.supplierid\n' +
        '\tJOIN public.customers cust ON cust.customerid = ord.customerid\n' +
        'WHERE\n' +
        '\tord.freight = 12.34\n' +
        "\tAND (cust.country = ''';DROPtest' or supp.companyname = ''';DROPtest')\n" +
        '\tAND ord.freight <> 12.34\n' +
        "\tAND (cust.country <> ''';DROPtest' or supp.companyname <> ''';DROPtest')\n" +
        "\tAND (cast(cust.country as varchar) like (replace(replace(replace('%'';DROPtest!', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!' or cast(supp.companyname as varchar) like (replace(replace(replace('%'';DROPtest!', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!')\n" +
        "\tAND (not(cast(cust.country as varchar) like (replace(replace(replace('%'';DROPtest!', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!') and not(cast(supp.companyname as varchar) like (replace(replace(replace('%'';DROPtest!', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!'))\n" +
        "\tAND (ord.shippeddate < '2000-10-28' and ord.shippeddate < '2000-10-28')\n" +
        "\tAND (ord.shippeddate <= '2000-10-28' and ord.shippeddate <= '2000-10-28')\n" +
        "\tAND (ord.shippeddate > '2000-10-28' and ord.shippeddate > '2000-10-28')\n" +
        "\tAND (ord.shippeddate >= '2000-10-28' and ord.shippeddate >= '2000-10-28')\n" +
        "\tAND (ord.shippeddate < '2000-10-28' or ord.shippeddate < '2000-10-28')\n" +
        "\tAND (ord.shippeddate <= '2000-10-28' or ord.shippeddate <= '2000-10-28')\n" +
        "\tAND (ord.shippeddate > '2000-10-28' or ord.shippeddate > '2000-10-28')\n" +
        "\tAND (ord.shippeddate >= '2000-10-28' or ord.shippeddate >= '2000-10-28')\n" +
        'GROUP BY order_shippeddate_1, customer_country_2, supplier_companyname_3, order_freight_4\n';

      const resp = await api.data.getQuery(req);
      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.text).to.equal(expResult);
    });

    it('Should build all types of "singleKeys" filters', async () => {
      const req: IDataRequest = {
        requestID: 'abc-123',
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [
          northWind.dimensions.order_shippeddate,
          northWind.dimensions.customer_country,
          northWind.dimensions.supplier_companyname,
          northWind.dimensions.order_freight,
        ],
        metrics: [northWind.metrics.order_orderscount],
        filters: {
          singleKeys: [
            {
              key: northWind.dimensions.order_freight,
              operation: "EQUALS",
              values: [12.34]
            },
            {
              key: northWind.dimensions.customer_country,
              operation: "EQUALS",
              values: ["';DROPtest", "test"]
            },
            {
              key: northWind.dimensions.order_freight,
              operation: "NOT_EQUALS",
              values: [12.34]
            },
            {
              key: northWind.dimensions.customer_country,
              operation: "NOT_EQUALS",
              values: ["';DROPtest", "test"]
            },
            {
              key: northWind.dimensions.customer_country,
              operation: "STARTS_WITH",
              values: ["%';DROPtest!", "test"]
            },
            {
              key: northWind.dimensions.customer_country,
              operation: "NOT_STARTS_WITH",
              values: ["%';DROPtest!", "test"]
            },
            {
              key: northWind.dimensions.order_shippeddate,
              operation: "ALL_IS_LESS",
              values: ["2000-10-28", "2002-11-11"]
            },
            {
              key: northWind.dimensions.order_shippeddate,
              operation: "ALL_IS_LESS_OR_EQUAL",
              values: ["2000-10-28", "2002-11-11"]
            },
            {
              key: northWind.dimensions.order_shippeddate,
              operation: "ALL_IS_MORE",
              values: ["2000-10-28", "2002-11-11"]
            },
            {
              key: northWind.dimensions.order_shippeddate,
              operation: "ALL_IS_MORE_OR_EQUAL",
              values: ["2000-10-28", "2002-11-11"]
            },
            {
              key: northWind.dimensions.order_shippeddate,
              operation: "AT_LEAST_ONE_IS_LESS",
              values: ["2000-10-28", "2002-11-11"]
            },
            {
              key: northWind.dimensions.order_shippeddate,
              operation: "AT_LEAST_ONE_IS_LESS_OR_EQUAL",
              values: ["2000-10-28", "2002-11-11"]
            },
            {
              key: northWind.dimensions.order_shippeddate,
              operation: "AT_LEAST_ONE_IS_MORE",
              values: ["2000-10-28", "2002-11-11"]
            },
            {
              key: northWind.dimensions.order_shippeddate,
              operation: "AT_LEAST_ONE_IS_MORE_OR_EQUAL",
              values: ["2000-10-28", "2002-11-11"]
            }
          ],
        },
      };

      const expResult =
        'SELECT -- rqid=abc-123 \n' +
        '\tord.shippeddate AS order_shippeddate_1,\n' +
        '\tcust.country AS customer_country_2,\n' +
        '\tsupp.companyname AS supplier_companyname_3,\n' +
        '\tord.freight AS order_freight_4,\n' +
        '\tSUM(1) AS order_orderscount_5\n' +
        'FROM public.orders AS ord\n' +
        '\tJOIN public.customers cust ON cust.customerid = ord.customerid\n' +
        '\tJOIN public.order_details ord_det ON ord_det.orderid = ord.orderid\n' +
        '\tJOIN public.products prd ON prd.productid = ord_det.productid\n' +
        '\tJOIN public.suppliers supp ON supp.supplierid = prd.supplierid\n' +
        'WHERE\n' +
        '\tord.freight = 12.34\n' +
        "\tAND cust.country in (''';DROPtest', 'test')\n" +
        '\tAND ord.freight <> 12.34\n' +
        "\tAND cust.country not in (''';DROPtest', 'test')\n" +
        "\tAND (cast(cust.country as varchar) like (replace(replace(replace('%'';DROPtest!', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!' or cast(cust.country as varchar) like (replace(replace(replace('test', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!')\n" +
        "\tAND (not(cast(cust.country as varchar) like (replace(replace(replace('%'';DROPtest!', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!') and not(cast(cust.country as varchar) like (replace(replace(replace('test', '!', '!!'), '%', '!%'), '_', '!_') || '%') escape '!'))\n" +
        "\tAND (ord.shippeddate < '2000-10-28' and ord.shippeddate < '2002-11-11')\n" +
        "\tAND (ord.shippeddate <= '2000-10-28' and ord.shippeddate <= '2002-11-11')\n" +
        "\tAND (ord.shippeddate > '2000-10-28' and ord.shippeddate > '2002-11-11')\n" +
        "\tAND (ord.shippeddate >= '2000-10-28' and ord.shippeddate >= '2002-11-11')\n" +
        "\tAND (ord.shippeddate < '2000-10-28' or ord.shippeddate < '2002-11-11')\n" +
        "\tAND (ord.shippeddate <= '2000-10-28' or ord.shippeddate <= '2002-11-11')\n" +
        "\tAND (ord.shippeddate > '2000-10-28' or ord.shippeddate > '2002-11-11')\n" +
        "\tAND (ord.shippeddate >= '2000-10-28' or ord.shippeddate >= '2002-11-11')\n" +
        'GROUP BY order_shippeddate_1, customer_country_2, supplier_companyname_3, order_freight_4\n';

      const resp = await api.data.getQuery(req);
      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.text).to.equal(expResult);
    });

    it('Should build with multiple "dateRanges" filters', async () => {
      const req: IDataRequest = {
        requestID: 'abc-123',
        scopeName: northWind.scope,
        dataModelName: northWind.dataModel,
        dimensions: [
          northWind.dimensions.order_shippeddate,
        ],
        metrics: [northWind.metrics.order_orderscount],
        filters: {
          dateRanges: [
            {
              key: northWind.dimensions.order_shippeddate,
              from: "1988-01-01"
            },
            {
              key: northWind.dimensions.order_shippeddate,
              to: "2088-01-01"
            },
            {
              key: northWind.dimensions.order_shippeddate,
              from: "1988-01-01",
              to: "2088-01-01"
            }
          ],
        },
      };

      const expResult =
        'SELECT -- rqid=abc-123 \n' +
        '\tord.shippeddate AS order_shippeddate_1,\n' +
        '\tSUM(1) AS order_orderscount_2\n' +
        'FROM public.orders AS ord\n' +
        'WHERE\n' +
        "\tord.shippeddate >= '1988-01-01'\n" +
        "\tAND ord.shippeddate <= '2088-01-01'\n" +
        "\tAND ord.shippeddate >= '1988-01-01'\n" +
        "\tAND ord.shippeddate <= '2088-01-01'\n" +
        'GROUP BY order_shippeddate_1\n';

      const resp = await api.data.getQuery(req);
      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.text).to.equal(expResult);
    });
  });

  describe('Getting data', () => {
    const commonReq: IDataRequest = {
      requestID: 'abc-123',
      scopeName: northWind.scope,
      dataModelName: northWind.dataModel,
      dimensions: [
        northWind.dimensions.order_shippeddate,
        northWind.dimensions.customer_country,
        northWind.dimensions.supplier_companyname,
        northWind.dimensions.order_freight,
      ],
      metrics: [northWind.metrics.order_orderscount],
      filters: {
        singleValues: [
          {
            keys: [northWind.dimensions.order_freight],
            operation: "ALL_IS_MORE",
            value: 3.25
          },
          {
            keys: [northWind.dimensions.customer_country],
            operation: "EQUALS",
            value: "Mexico"
          }
        ]
      },
      options: {
        rows: {
          startWithRow: 5,
          limitRowsTo: 10
        }
      }
    };

    it('Should get full data request', async () => {
      const resp = await api.data.get(commonReq);
      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.body.requestID).to.equal(commonReq.requestID);
      expect(resp.body.rows.length).to.equal(commonReq.options.rows.limitRowsTo);
      expect(resp.body.columnHeaders.length).to.equal(commonReq.dimensions.length + commonReq.metrics.length);
      expect(resp.body.totalRows).to.equal(commonReq.options.rows.limitRowsTo);

      expect(resp.body.columnHeaders[0]).to.include.all.keys([
        'title', 'name', 'dataType', 'alias', 'columnType',
      ]);

      for (const row of resp.body.rows) {
        expect(row).to.include.all.keys([
          northWind.dimensions.order_shippeddate,
          northWind.dimensions.supplier_companyname,
          northWind.metrics.order_orderscount
        ]);
        expect(row.customer_country).to.equal('Mexico');
        expect(row.order_freight).to.be.gte(3.25);
      }
    });

    it('Should get optimized data request', async () => {
      const resp = await api.data.getOptimized(commonReq);
      expect(resp.status).to.equal(200, formatFailedResponse(resp));
      expect(resp.body.requestID).to.equal(commonReq.requestID);
      expect(resp.body.rows.length).to.equal(commonReq.options.rows.limitRowsTo);
      expect(resp.body.columnHeaders.length).to.equal(commonReq.dimensions.length + commonReq.metrics.length);
      expect(resp.body.totalRows).to.equal(commonReq.options.rows.limitRowsTo);

      expect(resp.body.columnHeaders[0]).to.include.all.keys([
        'title', 'name', 'dataType', 'alias', 'columnType',
      ]);

      for (const row of resp.body.rows) {
        expect(row.length).to.eq(5);
        expect(row[1]).to.equal('Mexico');
        expect(row[3]).to.be.gte(3.25);
      }
    });

    it('Should get CSV data request', async () => {
      const resp = await api.data.getFile(commonReq);
      expect(resp.status).to.equal(200, formatFailedResponse(resp));

      const csvStr = resp.body.toString();
      const rows = csvStr.split('\n');
      expect(rows.length).to.equal(commonReq.options.rows.limitRowsTo + 2);

      expect(rows[0]).to.equal('"order_shippeddate","customer_country","supplier_companyname","order_freight","order_orderscount"');

      for (let i = 1; i < rows.length - 1; i += 1) {
        const row = rows[i];
        const values = row.split(',');

        expect(values[1]).to.equal('"Mexico"');
      }
    });
  });
});
