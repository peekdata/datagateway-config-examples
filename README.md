# Configuration Examples

This project contains examples of configuration files for [Peekdata Data API](https://peekdata.io/data-api) engine.
  - [Northwind example](./northwind/readme.md)
  - [Loans example](./loans/readme.md)

For the detailed explanation refer to [our documentation](https://peekdata.io/developers/index.html#/) or contact our team via [info@peekdata.io](mailto:info@peekdata.io).




