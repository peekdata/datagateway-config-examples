# Northwind Database Example <!-- omit in toc -->

- [Intro](#intro)
- [Setup the example](#setup-the-example)
  - [Starting Northwind DB and Data API](#starting-northwind-db-and-data-api)
  - [Running some reports](#running-some-reports)
    - [Retrieving most popular suppliers](#retrieving-most-popular-suppliers)
    - [Retrieving most profitable employees](#retrieving-most-profitable-employees)
    - [Retrieving revenue per country](#retrieving-revenue-per-country)
    - [Retrieving orders late on shipping per day](#retrieving-orders-late-on-shipping-per-day)
  - [Connecting Report Builder component to Data API](#connecting-report-builder-component-to-data-api)
- [Summary](#summary)

## Intro

The following example demonstrates how to setup a Data API with a Northwind database schema (widely known small-business ERP sample schema data first introduced by Microsoft). It contains the sales data for a fictitious company called “Northwind Traders,” which imports and exports specialty foods from around the world.

![mysql_northwind](https://user-images.githubusercontent.com/22106596/58474833-f3db9200-8154-11e9-9382-d716c9bf36d2.png)

In this example, we are using Docker to initiate both the PostgreSQL database and Data API with predefined configuration.

## Setup the example

### Starting Northwind DB and Data API

Clone this repository

```bash
git clone git@gitlab.com:peekdata/datagateway-config-examples.git && cd datagateway-config-examples
```

Change your current directory to the northwind example subfolder

```bash
cd northwind
```

Start docker containers with underlying docker images defined in `docker-compose.yml`

```bash
docker-compose up
```

After executing all of the above steps you will have Peekdata Data API running at url [http://localhost:8080/datagateway/v1](http://localhost:8080/datagateway/v1)

To verify that service is running and configuration loaded correctly check the `v1/healthcheck` endpoint result

```bash
curl http://localhost:8080/datagateway/v1/healthcheck
```

Expected result
```json
${"message":"Service is OK"}
```

You can find all available API endpoints in our documentation:
- [Data model exploring endpoints](https://peekdata.io/developers/index.html#/api/data-model-api)
- [Data retrieval endpoints](https://peekdata.io/developers/index.html#/api/data-api?id=getting-the-data)

### Running some reports

You are now ready to start using the API according to our predefined configuration of Northwind Example database.

We will be showing several examples of data retrieval using `curl`.

#### Retrieving most popular suppliers

```bash
curl -X \
    POST 'http://localhost:8080/datagateway/v1/data/get' \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    --data-binary '{
        "scopeName": "Northwind Marketing",
        "dataModelName": "Sales",
        "dimensions": ["supplier_companyname", "order_ship_country"],
        "metrics": ["order_productsalescount"],
        "options": {
            "rows": {
                "limitRowsTo": 100
            }
        },
        "sortings": {
            "metrics": [{
                "key": "order_productsalescount",
                "direction": "DESC"
            }]
        }
    }'
```

Let's say we need to filter the report results and find only orders that are shipped into "Mexico" no additional coding is necessary. To do so we need to add `filters` to your request payload as shown in the example below.

```bash
curl -X \
    POST 'http://localhost:8080/datagateway/v1/data/get' \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    --data-binary '{
        "scopeName": "Northwind Marketing",
        "dataModelName": "Sales",
        "dimensions": ["supplier_companyname", "order_ship_country"],
        "metrics": ["order_productsalescount"],
        "options": {
            "rows": {
                "limitRowsTo": 100
            }
        },
        "sortings": {
            "metrics": [{
                "key": "order_productsalescount",
                "direction": "DESC"
            }]
        },
        "filters": {
          "singleValues": [
            {
              "keys": ["order_ship_country"],
              "operation": "EQUALS",
              "value": "Mexico"
            }
          ]
        }
      }'
```


#### Retrieving most profitable employees
```bash
curl -X \
    POST 'http://localhost:8080/datagateway/v1/data/get' \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    --data-binary '{
        "scopeName": "Northwind Marketing",
        "dataModelName": "Sales",
        "dimensions": ["employee_firstname", "employee_lastname"],
        "metrics": ["totalprice"],
        "options": {
            "rows": {
                "limitRowsTo": 10
            }
        },
        "sortings": {
            "metrics": [{
                "key": "totalprice",
                "direction": "DESC"
            }]
        }
    }'
```

#### Retrieving revenue per country

```bash
curl -X \
    POST 'http://localhost:8080/datagateway/v1/data/get' \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    --data-binary '{
        "scopeName": "Northwind Marketing",
        "dataModelName": "Sales",
        "dimensions": ["customer_country"],
        "metrics": ["totalprice"],
        "sortings": {
            "metrics": [{
                "key": "totalprice",
                "direction": "DESC"
            }]
        }
    }'
```

#### Retrieving orders late on shipping per day

```bash
curl -X \
    POST 'http://localhost:8080/datagateway/v1/data/get' \
    -H 'Accept: application/json' \
    -H 'Content-Type: application/json' \
    --data-binary '{
        "scopeName": "Northwind Marketing",
        "dataModelName": "Sales",
        "dimensions": ["order_shippeddate"],
        "metrics": ["order_orderscount"],
        "filters": {
            "singleValues": [
                {
                    "keys": ["order_islate"],
                    "operation": "EQUALS",
                    "value": "Y"
                }
            ]
        }
    }'
```

### Connecting Report Builder component to Data API

To better understand what our Data API is we welcome you to try out our [React Report Builder](https://www.npmjs.com/package/react-report-builder). You can either build your react app and using this report builder component or you can follow this guide to run our [example report builder](https://www.npmjs.com/package/react-report-builder)

You will need to clone the above-mentioned repository

```bash
git clone git@gitlab.com:peekdata/react-report-builder-example.git && cd react-report-builder-example
```

Install all dependencies

```bash
npm install
```

Make a change in `app/index.tsx` file at `line 28` by setting the `baseUrl` variable value to your local API address - `http://localhost:8080/datagateway/v1`

Run the sample application

```bash
npm start
```

The application will run at [http://localhost:3000] allowing you to generate on the fly reports according to Northwind data model configuration.

Now you can try to build all of the same reports using the UI component

## Summary

The Data API can be used in more elaborate use cases than only reporting and our team will be glad to demonstrate other use cases. Feel free to contact us via if you will require any technical help [info@peekdata.io](mailto:info@peekdata.io).

You can read more about the product capabilities in our documentation section - [Data API](https://peekdata.io/developers/index.html#/)
