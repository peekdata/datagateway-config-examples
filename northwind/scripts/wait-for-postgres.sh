#!/bin/sh
# wait-for-postgres.sh

until PGPASSWORD=docker psql -h database -d "northwind" -U "postgres" -c '\q'; do
  >&2 echo "Postgres is unavailable - sleeping"
  sleep 5
done
  
>&2 echo "Postgres is up - executing command"
exec catalina.sh run